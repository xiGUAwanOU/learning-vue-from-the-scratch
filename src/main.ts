import Vue from 'vue';

Vue.component('some-vue-content', {
  template: `
    <div class="some-vue-content">
      <h2>This Part is Rendered by Vue</h2>
      <p>This is the content of the child component: {{ param }}.</p>
    </div>
  `,

  props: {
    param: { type: String, default: 'dummy content' },
  },
});

new Vue({
  el: '#site',
  data() {
    return {
      someContent: 'This is the content of a data field.',
    };
  },
});

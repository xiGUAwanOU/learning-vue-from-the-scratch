# learning-vue-from-the-scratch

This repo shows how to use Vue as a library instead of a framework. Vue will grab whatever inside the DOM tree and take the HTML content as the template and try to render it.

However, this approach requires runtime template rendering mechanism, which makes the vendor JS size bigger. But personally I find that this could make Vue.js a good extension for the already existing static website.

const path = require('path');
const CopyWebpackPlugin = require('copy-webpack-plugin');

const commonConfig = {
  entry: './src/main.ts',
  output: {
    filename: 'bundle.js',
    path: path.resolve(__dirname, 'dist'),
  },
  module: {
    rules: [
      {
        test: /\.ts$/,
        use: 'ts-loader',
        exclude: /node_modules/,
      },
      {
        test: /\.css$/,
        use: [ 'style-loader', 'css-loader' ],
      },
    ],
  },
  resolve: {
    extensions: [ '.ts', '.js' ],
    alias: {
      vue: 'vue/dist/vue.esm.js',
    },
  },
  plugins: [
    new CopyWebpackPlugin({
      patterns: [ 'public' ],
    }),
  ],
};

const devConfig = {
  mode: 'development',
  devtool: 'inline-source-map',
  devServer: {
    contentBase: './dist',
  },
};

const prodConfig = {
  mode: 'production',
};

module.exports = (env, argv) => {
  const mode = argv.mode || 'development';

  switch(mode) {
    case 'production':
      return Object.assign(commonConfig, prodConfig);

    case 'development':
    default:
      return Object.assign(commonConfig, devConfig);
  }
};
